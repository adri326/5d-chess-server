const rankingInterface = require('./rankingInterface');

const rankTransform = (rank) => {
  return {
    id: rank.id,
    username: rank.username,
    rating: rank.rating,
    variant: rank.variant,
    format: rank.format,
    date: rank.date,
    gameId: rank.gameId
  };
};

exports.getInfo = async (req, res) => {
  try {
    var ranking = (await rankingInterface.getRanking(req.params.id));
    res.status(200).send(rankTransform(ranking));
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} }); }
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') { req.body = {}; }
    var rankings = (await rankingInterface.getRankings(req.body)).map(e => rankTransform(e));
    res.status(200).send(rankings);
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} }); }
  }
};
