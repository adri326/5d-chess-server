const { spawn, Thread, Worker } = require('threads');
const gameInterface = require('./gameInterface');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');
var chess = null;

const initWorker = async () => {
  if(chess === null) {
    chess = await spawn(new Worker('./chessWorker'));
  }
};

const formatTransform = (timed) => {
  if(timed === null) {
    return 'untimed';
  }
  else {
    var res = '';
    res += Math.floor(timed.startingDuration / 60) + ':' + Math.floor(timed.startingDuration % 60).toFixed().padStart(2, '0');
    if(timed.perActionFlatIncrement > 0) { res += ';incf' + Math.floor(timed.perActionFlatIncrement); }
    if(timed.perActionTimelineIncrement > 0) { res += ';inct' + Math.floor(timed.perActionTimelineIncrement); }
    if(
      timed.startingDuration === 5 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 0
    ) {
      res = 'bullet';
    }
    if(
      timed.startingDuration === 10 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 5
    ) {
      res = 'blitz';
    }
    if(
      timed.startingDuration === 20 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 10
    ) {
      res = 'rapid';
    }
    if(
      timed.startingDuration === 40 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 20
    ) {
      res = 'standard';
    }
    if(
      timed.startingDuration === 80 * 60 &&
      timed.perActionFlatIncrement === 0 &&
      timed.perActionTimelineIncrement === 40
    ) {
      res = 'tournament';
    }
    return res;
  }
};

const onEnd = async (ses) => {
  if(ses.ended) {
    await initWorker();
    var exportData = await chess.exportSession(ses);
    await gameInterface.newGame(exportData);
  }
};

exports.authHost = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(username === 'admin') { return true; }
    return (username === existingSession.host);
  }
  else {
    throw 'Session not found!';
  }
};

exports.authNonHost = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(username === 'admin') { return true; }
    return (username !== existingSession.host) && (username === existingSession.black || username === existingSession.white);
  }
  else {
    throw 'Session not found!';
  }
};

exports.authPlayer = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(username === 'admin') { return true; }
    return ((username === existingSession.black && existingSession.player === 'black') || (username === existingSession.white && existingSession.player === 'white'));
  }
  else {
    throw 'Session not found!';
  }
};

exports.newSession = async (username, player = 'white', variant = 'standard', ranked = false, timed = null) => {
  await initWorker();
  var newSession = await chess.createSession(variant);
  var format = formatTransform(timed);
  if((format.includes(':') || format.includes('untimed')) && ranked) {
    throw 'Ranked must use standard time controls!';
  }
  newSession = await chess.updateSession(newSession, {
    host: username,
    white: player === 'white' ? username : null,
    black: player !== 'white' ? username : null,
    ranked: ranked,
    timed: timed,
    format: format
  });
  newSession.id = uuidv5(newSession.host + newSession.variant + newSession.format + (Date.now()), '97d87037-c436-4f77-8858-d0ab10d5085e');
  var ret = (await collections.sessions.insert(newSession));
  var copy = Object.assign({}, ret);
  delete copy.board;
  delete copy.actionHistory;
  delete copy.moveBuffer;
  delete copy.timed;
  console.log('New session created:');
  console.table(copy);
  return ret;
};

exports.removeSession = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      await collections.sessions.removeOne({ id: id });
      return null;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.addUser = async (id, username) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      if(existingSession.white !== null) {
        existingSession.black = username;
      }
      else {
        existingSession.white = username;
      }
      delete existingSession._id;
      existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionReady = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      existingSession.ready = true;
      delete existingSession._id;
      existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionUnready = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    if(!existingSession.started) {
      existingSession.ready = false;
      delete existingSession._id;
      existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
      return existingSession;
    }
    else {
      throw 'Session started already!';
    }
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionStart = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.startSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionMove = async (id, move) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.moveSession(existingSession, move);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionUndo = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.undoSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionSubmit = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.submitSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionForfeit = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.forfeitSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionOfferDraw = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.getSession(existingSession);
    existingSession.offerDraw = true;
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.sessionDraw = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.drawSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.getSession = async (id) => {
  var existingSession = await collections.sessions.findOne({ id: id });
  if(existingSession !== null) {
    await initWorker();
    var ended = existingSession.ended;
    existingSession = await chess.getSession(existingSession);
    if(!ended) { onEnd(existingSession); }
    delete existingSession._id;
    existingSession = await collections.sessions.update({ id: id }, { $set: existingSession });
    return existingSession;
  }
  else {
    throw 'Session not found!';
  }
};

exports.getSessions = async (query = {}) => {
  var res = [];
  var existingSessions = await collections.sessions.find(query);
  for(var i = 0;i < existingSessions.length;i++) {
    await initWorker();
    var ended = existingSessions[i].ended;
    var tmp = await chess.getSession(existingSessions[i]);
    if(!ended) { onEnd(tmp); }
    delete existingSession._id;
    await collections.sessions.update({ id: id }, { $set: existingSession });
    res.push(tmp);
  }
  return res;
};
