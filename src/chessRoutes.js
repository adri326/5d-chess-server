const chessInterface = require('./chessInterface');
const userInterface = require('./userInterface');

const sessionTransform = (ses) => {
  return {
    id: ses.id,
    host: ses.host,
    white: ses.white,
    black: ses.black,
    variant: ses.variant,
    format: ses.format,
    ranked: ses.ranked,
    ready: ses.ready,
    offerDraw: ses.offerDraw,
    started: ses.started,
    startDate: ses.started ? ses.startDate : 0,
    ended: ses.ended,
    endDate: ses.endDate,
    archiveDate: ses.archiveDate,
    timed: (ses.timed ?
      {
        whiteDurationLeft: ses.timed.whiteDurationLeft,
        blackDurationLeft: ses.timed.blackDurationLeft,
        startingDuration: ses.timed.startingDuration,
        perActionFlatIncrement: ses.timed.perActionFlatIncrement,
        perActionTimelineIncrement: ses.timed.perActionTimelineIncrement
      }
    :
      null
    ),
    board: ses.board,
    actionHistory: ses.actionHistory,
    moveBuffer: ses.moveBuffer,
    player: ses.player,
    winner: ses.winner,
    winCause: ses.winCause
  };
};

exports.new = async (req, res) => {
  var data = req.body;
  var timed = null;
  var valid = true;
  var error = '';
  if(data.timed !== null && typeof data.timed !== 'undefined') {
    if(typeof data.timed.startingDuration !== 'number') {
      error = 'Timed parameter is invalid, startingDuration must be a number!';
      valid = false;
    }
    else if(data.timed.startingDuration < 0) {
      error = 'Timed parameter is invalid, startingDuration must be greater than or equal to zero!';
      valid = false;
    }
    if(typeof data.timed.perActionFlatIncrement !== 'number') {
      error = 'Timed parameter is invalid, perActionFlatIncrement must be a number!';
      valid = false;
    }
    else if(data.timed.perActionFlatIncrement < 0) {
      error = 'Timed parameter is invalid, perActionFlatIncrement must be greater than or equal to zero!';
      valid = false;
    }
    if(typeof data.timed.perActionTimelineIncrement !== 'number') {
      error = 'Timed parameter is invalid, perActionTimelineIncrement must be a number!';
      valid = false;
    }
    else if(data.timed.perActionTimelineIncrement < 0) {
      error = 'Timed parameter is invalid, perActionTimelineIncrement must be greater than or equal to zero!';
      valid = false;
    }
    if(valid) {
      timed = {
        whiteDurationLeft: Math.floor(data.timed.startingDuration),
        blackDurationLeft: Math.floor(data.timed.startingDuration),
        startingDuration: Math.floor(data.timed.startingDuration),
        perActionFlatIncrement: Math.floor(data.timed.perActionFlatIncrement),
        perActionTimelineIncrement: Math.floor(data.timed.perActionTimelineIncrement),
        lastUpdate: 0
      };
    }
  }
  else if(typeof data.format === 'string') {
    if(data.format === 'bullet') {
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 5 * 60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 0,
        lastUpdate: 0
      };
    }
    else if(data.format === 'blitz') {
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 10 * 60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 5,
        lastUpdate: 0
      };
    }
    else if(data.format === 'rapid') {
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 20 * 60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 10,
        lastUpdate: 0
      };
    }
    else if(data.format === 'standard') {
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 40 * 60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 20,
        lastUpdate: 0
      };
    }
    else if(data.format === 'tournament') {
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 80 * 60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 40,
        lastUpdate: 0
      };
    }
    else {
      error = 'Format field is not valid! Must be \'bullet\', \'blitz\', \'rapid\', \'standard\', or \'tournament\'.';
      valid = false;
    }
  }
  if(valid && data.player !== 'white' && data.player !== 'black') {
    error = 'Player field is not valid! Must be \'white\' or \'black\'.';
    valid = false;
  }
  if(valid && typeof data.variant !== 'string') {
    data.variant = 'standard';
  }
  if(valid && typeof data.variant === 'string') {
    if(
      data.variant !== 'standard' &&
      data.variant !== 'defended_pawn' &&
      data.variant !== 'half_reflected'
    ) {
      error = 'Variant field is not valid! Must be \'standard\', \'defended_pawn\', or \'half_reflected\'.';
      valid = false;
    }
  }
  if(valid && typeof data.ranked !== 'undefined') {
    if(typeof data.ranked !== 'boolean') {
      error = 'Ranked field is not boolean!';
      valid = false;
    }
  }
  if(valid) {
    try {
      var newSession = await chessInterface.newSession(req.username, data.player, data.variant, data.ranked, timed);
      res.status(200).send(sessionTransform(newSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.remove = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not host!' });
  }
  if(valid) {
    try {
      await chessInterface.removeSession(req.params.id);
      res.status(200).end();
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.addUser = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && typeof data.username === 'string') {
    if(data.username !== req.username) {
      try {
        await userInterface.getUser(data.username);
      }
      catch(err) {
        error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
        valid = false;
      }
    }
    else {
      valid = false;
      error = 'Username cannot be same as host!'
    }
  }
  else {
    valid = false;
    error = 'Username parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not host!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.addUser(req.params.id, data.username);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.ready = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authNonHost(req.params.id, req.username))) {
    res.status(403).send({ error: 'User must be non-host player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionReady(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.unready = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  else {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authNonHost(req.params.id, req.username))) {
    res.status(403).send({ error: 'User must be non-host player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionUnready(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.start = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authHost(req.params.id, req.username))) {
    res.status(403).send({ error: 'User must be host!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionStart(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.move = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && typeof data.move === 'object') {
    valid = false;
    error = 'Move parameter is not an object!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionMove(req.params.id, data);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.undo = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionUndo(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.submit = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionSubmit(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.forfeit = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid && !(await chessInterface.authPlayer(req.params.id, req.username))) {
    res.status(403).send({ error: 'User is not current player!' });
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.sessionForfeit(req.params.id);
      res.status(200).send(sessionTransform(existingSession));
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.draw = async (req, res) => {
  var data = req.body;
  var valid = true;
  var error = '';
  if(typeof req.params.id !== 'string') {
    valid = false;
    error = 'Session id parameter is not a string!';
  }
  if(valid) {
    try {
      var existingSession = await chessInterface.getSession(req.params.id);
      if(!(await chessInterface.authPlayer(req.params.id, req.username)) && existingSession.offerDraw) {
        existingSession = await chessInterface.sessionDraw(req.params.id);
        res.status(200).send(sessionTransform(existingSession));
      }
      else if(await chessInterface.authPlayer(req.params.id, req.username)) {
        existingSession = await chessInterface.sessionOfferDraw(req.params.id);
        res.status(200).send(sessionTransform(existingSession));
      }
    }
    catch(err) {
      error = typeof err === 'string' ? err : {message: err.message, stack: err.stack};
      valid = false;
    }
  }
  if(!valid) {
    if(!res.headersSent) { res.status(403).send({ error: error }); }
  }
};

exports.getInfo = async (req, res) => {
  try {
    var session = (await chessInterface.getSession(req.params.id));
    res.status(200).send(sessionTransform(session));
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} });
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') { req.body = {}; }
    var sessions = (await chessInterface.getSessions(req.body)).map(e => sessionTransform(e));
    res.status(200).send(sessions);
  }
  catch(err) {
    res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} });
  }
};
