const config = require('./config');
var collections = require('./db').init();
const bcrypt = require('bcrypt');

exports.registerUser = async (username, password, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser === null) {
    var newUser = {
      username: username,
      passwordHash: (await bcrypt.hash(password, config.saltRounds ? config.saltRounds : 10)),
      fullname: '',
      bio: '',
      country: '',
      bot: true,
      joinDate: Date.now()
    };
    if(additionalInfo.fullname) { newUser.fullname = additionalInfo.fullname; }
    if(additionalInfo.bio) { newUser.bio = additionalInfo.bio; }
    if(additionalInfo.country) { newUser.country = additionalInfo.country; }
    if(typeof additionalInfo.bot !== 'undefined') { newUser.bot = additionalInfo.bot; }
    var ret = (await collections.users.insert(newUser));
    console.log('New user registered:');
    console.table(ret);
    return ret;
  }
  else {
    throw 'Username already exists!';
  }
};

exports.authUser = async (username, password) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    return (await bcrypt.compare(password, existingUser.passwordHash));
  }
  else {
    throw 'User not found!';
  }
};

exports.updateUser = async (username, additionalInfo = {}) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    if(additionalInfo.fullname) { existingUser.fullname = additionalInfo.fullname; }
    if(additionalInfo.bio) { existingUser.bio = additionalInfo.bio; }
    if(additionalInfo.country) { newUser.bio = additionalInfo.country; }
    if(typeof additionalInfo.bot !== 'undefined') { newUser.bot = additionalInfo.bot; }
    delete existingUser._id;
    await collections.users.update({ username: username }, { $set: existingUser });
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUser = async (username) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    return existingUser;
  }
  else {
    throw 'User not found!';
  }
};

exports.getUsers = async (query = {}) => {
  return await collections.users.find(query);
};
