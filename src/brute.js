const config = require('./config');
var ExpressBrute = require('express-brute');
/*
var MongoStore = require('express-brute-mongo');
var collections = require('./db').init();

var store = MongoStore((ready) => {
  ready(() => {collections.brute});
});
*/
var slow = new ExpressBrute.MemoryStore();
var bruteSlow = new ExpressBrute(slow, {
  freeRetries: 0,
  minWait: 5000,
  maxWait: 5000
});

var med = new ExpressBrute.MemoryStore();
var bruteMed = new ExpressBrute(med, {
  freeRetries: 0,
  minWait: 1000,
  maxWait: 1000
});

var fast = new ExpressBrute.MemoryStore();
var bruteFast = new ExpressBrute(fast, {
  freeRetries: 0,
  minWait: 250,
  maxWait: 250
});

if(config.enableBrute || typeof config.enableBrute === 'undefined') {
  module.exports = {
    slow: bruteSlow.prevent,
    med: bruteMed.prevent,
    fast: bruteFast.prevent
  };
}
else {
  module.exports = {
    slow: (req, res, next) => { next(); },
    med: (req, res, next) => { next(); },
    fast: (req, res, next) => { next(); }
  };
}
