const userInterface = require('./userInterface');
const secret = require('./secret');
const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

exports.init = async () => {
  try {
    var secretStr = await secret.getSecret();
    console.log('Secret is \"' + secretStr + '\"');
    var botStr = await secret.getNonbot();
    console.log('Bot Token is \"' + botStr + '\"');
    var adminPass = uuidv4();
    await userInterface.registerUser('admin', adminPass);
    console.log('Default admin account created, password is \"' + adminPass + '\"');
    try {
      fs.writeFileSync('secrets', 'Secret: ' + secretStr + '\nBot Token: ' + botStr + '\nAdmin Password: ' + adminPass);
      console.log('Passwords and secrets are saved in secrets file');
    }
    catch(err) {
      console.error(err);
    }
  }
  catch(err) {
    console.error(err);
  }
};
