const config = require('./config');
const init = require('./init');
const heartbeat = require('./heartbeat');
const app = require('./app');


const PORT = process.env.PORT || (config.port ? config.port : 5000);

init.init().then(() => {
  heartbeat.init();
  app.listen(PORT, () => {
    console.log('Server started\nListening on port ' + PORT);
  });
});
