const { expose } = require('threads/worker');
const Chess = require('5d-chess-js');
const config = require('./config');

const checkTime = (ses) => {
  if(ses.timed && !ses.ended && ses.started) {
    if(ses.player === 'white') {
      ses.timed.whiteDurationLeft = ses.timed.whiteDurationLeft - (Date.now() - ses.timed.lastUpdate)/1000;
      if(ses.timed.whiteDurationLeft <= 0) {
        ses.timed.whitekDurationLeft = 0;
        ses.winner = 'black';
        ses.winCause = 'timed_out';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
    }
    else {
      ses.timed.blackDurationLeft = ses.timed.blackDurationLeft - (Date.now() - ses.timed.lastUpdate)/1000;
      if(ses.timed.blackDurationLeft <= 0) {
        ses.timed.blackDurationLeft = 0;
        ses.winner = 'white';
        ses.winCause = 'timed_out';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
    }
    ses.timed.lastUpdate = Date.now();
  }
  return ses;
};

const sessionWorker = {
  createSession: (variant = 'standard') => {
    var ses = {
      host: null,
      white: null,
      black: null,
      variant: variant,
      format: null,
      ranked: false,
      ready: false,
      offerDraw: false,
      started: false,
      startDate: Date.now(),
      ended: false,
      endDate: 0,
      archiveDate: 0,
      timed: null,
      board: null,
      actionHistory: null,
      moveBuffer: null,
      player: 'white',
      winner: null,
      winCause: null
    };
    var chess = new Chess();
    ses.board = chess.board;
    ses.actionHistory = chess.actionHistory;
    ses.moveBuffer = chess.moveBuffer;
    return ses;
  },
  updateSession: (ses, info = null) => {
    /*
      timed = {
        whiteDurationLeft: 0,
        blackDurationLeft: 0,
        startingDuration: 10*60,
        perActionFlatIncrement: 0,
        perActionTimelineIncrement: 5,
        lastUpdate: Date.now()
      }
    */
    if(typeof info === 'object') {
      if(typeof info.host === 'string') { ses.host = info.host; }
      if(typeof info.white === 'string') { ses.white = info.white; }
      if(typeof info.black === 'string') { ses.black = info.black; }
      if(typeof info.variant === 'string') { ses.variant = info.variant; }
      if(typeof info.ranked === 'boolean') { ses.ranked = info.ranked; }
      if(typeof info.timed === 'object') { ses.timed = info.timed; }
      if(typeof info.format === 'string') { ses.format = info.format; }
      if(typeof info.board === 'object') { ses.board = info.board; }
      if(Array.isArray(info.actionHistory)) { ses.actionHistory = info.actionHistory; }
      if(Array.isArray(info.moveBuffer)) { ses.moveBuffer = info.moveBuffer; }
      if(typeof info.player === 'string') { ses.player = info.player; }
    }
    return ses;
  },
  startSession: (ses) => {
    if(ses.ready && !ses.winner && !ses.started) {
      ses.started = true;
      ses.startDate = Date.now();
      if(ses.timed) {
        ses.timed.whiteDurationLeft = ses.timed.startingDuration + ses.timed.perActionFlatIncrement + ses.timed.perActionTimelineIncrement * ses.board.timelines.filter(e => e.present).length;
        ses.timed.blackDurationLeft = ses.timed.startingDuration;
        ses.timed.lastUpdate = Date.now();
      }
    }
    else {
      throw 'Non-host player not ready!';
    }
    return ses;
  },
  getSession: (ses) => {
    ses = checkTime(ses);
    return ses;
  },
  moveSession: (ses, move) => {
    ses = checkTime(ses);
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.import(ses.actionHistory, ses.variant, true);
      for(var i = 0;i < ses.moveBuffer.length;i++) {
        chess.move(ses.moveBuffer[i]);
      }
      chess.move(move);
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
    }
    return ses;
  },
  undoSession: (ses) => {
    ses = checkTime(ses);
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.import(ses.actionHistory, ses.variant, true);
      for(var i = 0;i < ses.moveBuffer.length;i++) {
        chess.move(ses.moveBuffer[i]);
      }
      chess.undo();
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
    }
    return ses;
  },
  submitSession: (ses) => {
    ses = checkTime(ses);
    if(!ses.ended && ses.started) {
      var chess = new Chess();
      chess.checkmateTimeout = 10000;
      chess.import(ses.actionHistory, ses.variant, true);
      for(var i = 0;i < ses.moveBuffer.length;i++) {
        chess.move(ses.moveBuffer[i]);
      }
      chess.submit(true);
      ses.offerDraw = false;
      ses.board = chess.board;
      ses.actionHistory = chess.actionHistory;
      ses.moveBuffer = chess.moveBuffer;
      ses.player = chess.player;
      if(chess.inStalemate) {
        ses.winner = 'draw';
        ses.winCause = 'regular';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
      else if(chess.inCheckmate) {
        ses.winner = chess.player === 'white' ? 'black' : 'white';
        ses.winCause = 'regular';
        ses.ended = true;
        ses.endDate = Date.now();
        ses.archiveDate = Date.now() + config.archive;
      }
      else if(ses.timed) {
        if(ses.player === 'white') {
          ses.timed.whiteDurationLeft += ses.timed.perActionFlatIncrement + ses.timed.perActionTimelineIncrement * ses.board.timelines.filter(e => e.present).length;
        }
        else {
          ses.timed.blackDurationLeft += ses.timed.perActionFlatIncrement + ses.timed.perActionTimelineIncrement * ses.board.timelines.filter(e => e.present).length;
        }
        ses.timed.lastUpdate = Date.now();
      }
    }
    return ses;
  },
  forfeitSession: (ses) => {
    ses = checkTime(ses);
    if(!ses.ended && ses.started) {
      ses.winner = ses.player === 'white' ? 'black' : 'white';
      ses.winCause = 'forfeit';
      ses.ended = true;
      ses.endDate = Date.now();
      ses.archiveDate = Date.now() + config.archive;
    }
    return ses;
  },
  drawSession: (ses) => {
    ses = checkTime(ses);
    if(!ses.ended && ses.started) {
      ses.winner = 'draw';
      ses.winCause = 'forfeit';
      ses.ended = true;
      ses.endDate = Date.now();
      ses.archiveDate = Date.now() + config.archive;
    }
    return ses;
  },
  exportSession: (ses) => {
    ses = checkTime(ses);
    var exportData = {
      white: ses.white,
      black: ses.black,
      variant: ses.variant,
      format: ses.format,
      ranked: ses.ranked,
      startDate: ses.startDate,
      endDate: ses.endDate,
      actionHistory: ses.actionHistory,
      winner: ses.winner,
      winCause: ses.winCause
    };
    return exportData;
  }
};

expose(sessionWorker);
