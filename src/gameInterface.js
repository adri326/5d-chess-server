const rankingInterface = require('./rankingInterface');
const EloUtils = require('elo-utils');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');

exports.newGame = async (game) => {
  var newGame = Object.assign({}, game);
  newGame.id = uuidv5(newGame.variant + newGame.format + newGame.startDate, '2cdc4423-bb23-4e3c-b379-59f4a99d4f18');
  var ret = (await collections.games.insert(newGame));
  if(ret.ranked) {
    var whiteOverallRanking = await rankingInterface.getLatestRanking(ret.white, 'overall', ret.format);
    var whiteSpecificRanking = await rankingInterface.getLatestRanking(ret.white, ret.variant, ret.format);
    var blackOverallRanking = await rankingInterface.getLatestRanking(ret.black, 'overall', ret.format);
    var blackSpecificRanking = await rankingInterface.getLatestRanking(ret.black, ret.variant, ret.format);
    var eloResult = (ret.winner === 'draw' ?
      EloUtils.RESULT.TIE
    : ret.winner === 'white' ?
      EloUtils.RESULT.R1
    :
      EloUtils.RESULT.R2
    );
    var eloOverallRatings = EloUtils.elo(whiteOverallRanking.rating, blackOverallRanking.rating, eloResult);
    var eloSpecificRatings = EloUtils.elo(whiteSpecificRanking.rating, blackSpecificRanking.rating, eloResult);
    await rankingInterface.addRanking(ret.white, eloOverallRatings.R1, 'overall', ret.format, ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.black, eloOverallRatings.R2, 'overall', ret.format, ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.white, eloSpecificRatings.R1, ret.variant, ret.format, ret.id, ret.endDate);
    await rankingInterface.addRanking(ret.black, eloSpecificRatings.R2, ret.variant, ret.format, ret.id, ret.endDate);
  }
  var copy = Object.assign({}, ret);
  delete copy.actionHistory;
  console.log('New game created:');
  console.table(copy);
  return ret;
};

exports.getGame = async (id) => {
  var existingGame = await collections.games.findOne({ id: id });
  if(existingGame !== null) {
    return existingGame;
  }
  else {
    throw 'Game not found!';
  }
};

exports.getGames = async (query = {}) => {
  return (await collections.games.find(query));
};
