const gameInterface = require('./gameInterface');

const gameTransform = (game) => {
  return {
    id: game.id,
    white: game.white,
    black: game.black,
    variant: game.variant,
    format: game.format,
    ranked: game.ranked,
    startDate: game.startDate,
    endDate: game.endDate,
    actionHistory: game.actionHistory,
    winner: game.winner,
    winCause: game.winCause
  };
};

exports.getInfo = async (req, res) => {
  try {
    var game = (await gameInterface.getGame(req.params.id));
    res.status(200).send(gameTransform(game));
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} }); }
  }
};

exports.getInfoQuery = async (req, res) => {
  try {
    if(typeof req.body !== 'object') { req.body = {}; }
    var games = (await gameInterface.getGames(req.body)).map(e => gameTransform(e));
    res.status(200).send(games);
  }
  catch(err) {
    if(!res.headersSent) { res.status(500).send({ error: typeof err === 'string' ? err : {message: err.message, stack: err.stack} }); }
  }
};
