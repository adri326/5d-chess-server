var collections = require('./db').init();
const { v4: uuidv4 } = require('uuid');

const initSecret = async () => {
  var existingSecretObj = await collections.secrets.findOne({});
  if(existingSecretObj === null) {
    var secret = uuidv4() + uuidv4() + uuidv4() + uuidv4() + uuidv4();
    var nonbot = uuidv4();
    existingSecretObj = await collections.secrets.insert({
      secret: secret,
      nonbot: nonbot
    });
  }
  return existingSecretObj;
};

exports.getSecret = async () => {
  return (await initSecret()).secret;
};

exports.getNonbot = async () => {
  return (await initSecret()).nonbot;
};
