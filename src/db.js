const config = require('./config');
const Datastore = require('nedb-promises');
const mongoist = require('mongoist');
const fs = require('fs');

var collections = null;

exports.init = () => {
  if(collections === null) {
    if(!config.db || config.db === null) {
      try { fs.mkdirSync('./db'); } catch(err) {}
      collections = {
        users: Datastore.create({ filename: './db/users.nedb', autoload: true }),
        games: Datastore.create({ filename: './db/games.nedb', autoload: true }),
        sessions: Datastore.create({ filename: './db/sessions.nedb', autoload: true }),
        rankings: Datastore.create({ filename: './db/rankings.nedb', autoload: true }),
        secrets: Datastore.create({ filename: './db/secrets.nedb', autoload: true })
      };
    }
    else {
      if(config.db.type === 'mongodb') {
        var db = {};
        if(config.db.options) {
          db = mongoist(config.db.url, config.db.options);
        }
        else {
          db = mongoist(config.db.url);
        }
        collections = {
          users: db.users,
          games: db.games,
          sessions: db.sessions,
          rankings: db.rankings,
          secrets: db.secrets
        };
      }
    }
  }
  return collections;
};
