const secret = require('./secret');
const jwt = require('jsonwebtoken');
const userInterface = require('./userInterface');

exports.tokenSign = async (username) => {
  try {
    return jwt.sign({ username: username }, await secret.getSecret(), {
      expiresIn: '12h'
    });
  }
  catch(err) {
    throw err;
  }
};

exports.tokenVerify = async (token) => {
  try {
    return jwt.verify(token, await secret.getSecret(), {
      maxAge: '12h'
    });
  }
  catch(err) {
    throw err;
  }
};

exports.authRoute = async (req, res, next) => {
  req.username = null;
  if(!req.headers.authorization) {
    res.status(403).send({ error: 'No credentials sent!' });
  }
  else {
    try {
      req.username = (await this.tokenVerify(req.headers.authorization)).username;
    }
    catch(err) {
      res.status(401).send(err);
    }
  }
  next();
};
