const app = require('../app');
const supertest = require('supertest');
const request = supertest(app);

test('Play session', async () => {
  var user1 = await request.post('/login').send({
    username: 'user1',
    password: 'user1'
  });
  var user1Token = user1.text;
  var user2 = await request.post('/login').send({
    username: 'user2',
    password: 'user2'
  });
  var user2Token = user2.text;
  //User1 creates session
  var res = await request.post('/sessions/new').set('Authorization', user1Token).send({
    player: 'white'
  });
  var ses = res.body;
  //User1 adds user2 as a player
  res = await request.post('/sessions/' + ses.id + '/addUser').set('Authorization', user1Token).send({
    username: 'user2'
  });
  //User2 signals ready
  res = await request.post('/sessions/' + ses.id + '/ready').set('Authorization', user2Token).send({});
  //User1 starts game
  res = await request.post('/sessions/' + ses.id + '/start').set('Authorization', user1Token).send({});
  //User1 plays first action
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":1,"player":"white","coordinate":"e2","rank":2,"file":5},"end":{"timeline":0,"turn":1,"player":"white","coordinate":"e3","rank":3,"file":5},"player":"white"});
  expect(res.status).toBe(200);
  res = await request.post('/sessions/' + ses.id + '/undo').set('Authorization', user1Token).send({});
  expect(res.status).toBe(200);
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e2",
      "rank": 2,
      "file": 5
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e3",
      "rank": 3,
      "file": 5
    },
    "player": "white"
  });
  expect(res.status).toBe(200);
  res = await request.post('/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  expect(res.status).toBe(200);
  //User2 plays second action
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f7",
      "rank": 7,
      "file": 6
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f6",
      "rank": 6,
      "file": 6
    },
    "player": "black"
  });
  res = await request.post('/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  expect(res.status).toBe(200);
  //User1 plays third action
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"white","coordinate":"d1","rank":1,"file":4},"end":{"timeline":0,"turn":2,"player":"white","coordinate":"f3","rank":3,"file":6},"player":"white"});
  res = await request.post('/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  expect(res.status).toBe(200);
  //User2 plays four action
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"black","coordinate":"d7","rank":7,"file":4},"end":{"timeline":0,"turn":2,"player":"black","coordinate":"d6","rank":6,"file":4},"player":"black"});
  res = await request.post('/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  expect(res.status).toBe(200);
  //User1 plays fifth action
  res = await request.post('/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":3,"player":"white","coordinate":"f3","rank":3,"file":6},"end":{"timeline":0,"turn":3,"player":"white","coordinate":"h5","rank":5,"file":8},"player":"white"});
  res = await request.post('/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  expect(res.status).toBe(200);
  expect(res.body).toMatchObject({
    ended: true,
    winner: 'white',
    winCause: 'regular'
  });
});
