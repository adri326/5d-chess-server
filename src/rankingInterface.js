const config = require('./config');
var collections = require('./db').init();
const { v5: uuidv5 } = require('uuid');

const initRanking = async (username, variant, format) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    var initRanking = await collections.rankings.findOne({ username: username, variant: variant, format: format });
    if(initRanking === null) {
      await collections.rankings.insert({
        username: username,
        rating: config.startingElo,
        variant: variant,
        format: format,
        date: existingUser.joinDate,
        gameId: ''
      });
    }
  }
  else {
    throw 'User not found!';
  }
};

exports.addRanking = async (username, ranking, variant, format, gameId, date = null) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await initRanking(username, variant, format);
    var newRanking = {
      id: uuidv5(username + ranking + variant + format + (date ? date : Date.now()), '214fcb11-72b6-4d41-ab2e-797fc4ccae07'),
      username: username,
      rating: ranking,
      variant: variant,
      format: format,
      date: date ? date : Date.now(),
      gameId: gameId
    };
    return (await collections.rankings.insert(newRanking));
  }
  else {
    throw 'User not found!';
  }
};

exports.getLatestRanking = async (username, variant, format) => {
  var existingUser = await collections.users.findOne({ username: username });
  if(existingUser !== null) {
    await initRanking(username, variant, format);
    var rankings = await collections.rankings.find({ username: username, variant: variant, format: format }).sort({ date: -1 });
    return rankings[0];
  }
  else {
    throw 'User not found!';
  }
};

exports.getRanking = async (id) => {
  var existingRanking = await collections.rankings.findOne({ id: id });
  if(existingRanking !== null) {
    return existingRanking;
  }
  else {
    throw 'Ranking not found!';
  }
};

exports.getRankings = async (query = {}) => {
  return (await collections.rankings.find(query));
};
