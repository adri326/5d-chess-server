const config = require('./config');
const chessInterface = require('./chessInterface');

var interval = null;

const heartbeatFunc = async () => {
  var sessions = await chessInterface.getSessions();
  for(var i = 0;i < sessions.length;i++) {
    if(sessions[i].startDate + config.deadline <= Date.now()) {
      //Reached deadline
      chessInterface.removeSession(sessions[i].id);
    }
    else {
      if(sessions[i].ended && sessions[i].archiveDate <= Date.now()) {
        //Reached archive
        chessInterface.removeSession(sessions[i].id);
      }
    }
  }
};

exports.init = () => {
  if(interval === null) {
    heartbeatFunc();
    interval = setInterval(heartbeatFunc, config.heartbeat);
  }
};

exports.clear = () => {
  if(interval !== null) {
    clearInterval(interval);
  }
};
