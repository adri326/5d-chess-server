var config = null;

try {
  config = require('../config.json');
}
catch(err) {
  config = require('../defaultConfig.json');
}

module.exports = config;
