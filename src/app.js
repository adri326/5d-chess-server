const express = require('express');
const cors = require('cors');
const userRoutes = require('./userRoutes');
const chessRoutes = require('./chessRoutes');
const gameRoutes = require('./gameRoutes');
const rankingRoutes = require('./rankingRoutes');
const auth = require('./auth');
const brute = require('./brute');

var app = express();

//Allow CORS on all requests
app.use(cors());

//JSON parse middleware
app.use(express.json());

//User Auth
app.post('/register', brute.slow, userRoutes.register);
app.post('/login', brute.slow, userRoutes.login);
app.get('/refreshToken', brute.slow, auth.authRoute, userRoutes.refresh);

//User Info
app.get('/users/:username', brute.med, userRoutes.getInfo);
app.get('/users', brute.med, userRoutes.getInfoQuery);
app.post('/users', brute.med, userRoutes.getInfoQuery);
app.post('/users/:username/update', brute.slow, auth.authRoute, userRoutes.update);

//Session
app.get('/sessions/:id', brute.fast, chessRoutes.getInfo);
app.get('/sessions', brute.med, chessRoutes.getInfoQuery);
app.post('/sessions', brute.med, chessRoutes.getInfoQuery);
app.post('/sessions/new', brute.med, auth.authRoute, chessRoutes.new);
app.post('/sessions/:id/remove', brute.med, auth.authRoute, chessRoutes.remove);
app.post('/sessions/:id/addUser', brute.med, auth.authRoute, chessRoutes.addUser);
app.post('/sessions/:id/ready', brute.med, auth.authRoute, chessRoutes.ready);
app.post('/sessions/:id/unready', brute.med, auth.authRoute, chessRoutes.unready);
app.post('/sessions/:id/start', brute.med, auth.authRoute, chessRoutes.start);
app.post('/sessions/:id/move', brute.fast, auth.authRoute, chessRoutes.move);
app.post('/sessions/:id/undo', brute.fast, auth.authRoute, chessRoutes.undo);
app.post('/sessions/:id/submit', brute.fast, auth.authRoute, chessRoutes.submit);
app.post('/sessions/:id/forfeit', brute.fast, auth.authRoute, chessRoutes.forfeit);
app.post('/sessions/:id/draw', brute.fast, auth.authRoute, chessRoutes.draw);

//Game
app.get('/games/:id', brute.med, gameRoutes.getInfo);
app.get('/games', brute.med, gameRoutes.getInfoQuery);
app.post('/games', brute.med, gameRoutes.getInfoQuery);

//Ranking
app.get('/rankings/:id', brute.med, rankingRoutes.getInfo);
app.get('/rankings', brute.med, rankingRoutes.getInfoQuery);
app.post('/rankings', brute.med, rankingRoutes.getInfoQuery);

module.exports = app;
