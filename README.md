# 5D Chess Server

Open source implementation of '5D Chess With Multiverse Time Travel' as a REST style API server with ELO rankings.

Official server instance is https://server.chessin5d.net.

## Example

Bottom example uses [superagent](https://www.npmjs.com/package/superagent) as the AJAX HTTP request library.

``` js
const request = require('superagent');
const url = 'https://server.chessin5d.net';

const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds));
};

const main = async () => {
	//Registering test users
  try {
    await request.post(url + '/register').send({
      username: 'testuser1',
      password: 'testuser1',
      bio: 'Test User 1, not a real player',
      fullname: 'Test User 1'
    });
  }
  catch(err) {}
  await sleep(10000);
  try {
    await request.post(url + '/register').send({
      username: 'testuser2',
      password: 'testuser2',
      bio: 'Test User 2, not a real player',
      fullname: 'Test User 2'
    });
  }
  catch(err) {}
  await sleep(10000);
  
  //Login as test users and grab JWT
	var user1 = await request.post(url + '/login').send({
    username: 'testuser1',
    password: 'testuser1'
  });
  var user1Token = user1.text;
  console.log('Test User 1 JWT: ' + user1Token);
  await sleep(10000);
  var user2 = await request.post(url + '/login').send({
    username: 'testuser2',
    password: 'testuser2'
  });
  var user2Token = user2.text;
  console.log('Test User 2 JWT: ' + user2Token);
  await sleep(10000);
  
  //Testuser1 creates session
  var res = await request.post(url + '/sessions/new').set('Authorization', user1Token).send({
    player: 'white'
  });
  var ses = res.body;
  console.log('Session created:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
  
  //Testuser1 adds testuser2 as the other player
  res = await request.post(url + '/sessions/' + ses.id + '/addUser').set('Authorization', user1Token).send({
    username: 'testuser2'
  });
  await sleep(1000);
  
  //Testuser2 signals ready for game to start
  res = await request.post(url + '/sessions/' + ses.id + '/ready').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //Testuser1 starts game
  res = await request.post(url + '/sessions/' + ses.id + '/start').set('Authorization', user1Token).send({});
  ses = res.body;
  console.log('Session started:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
  
  //Testuser1 plays first action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e2",
      "rank": 2,
      "file": 5
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "white",
      "coordinate": "e3",
      "rank": 3,
      "file": 5
    },
    "player": "white"
  });
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  await sleep(1000);
  
  //User2 plays second action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({
    "promotion": null,
    "enPassant": null,
    "castling": null,
    "start": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f7",
      "rank": 7,
      "file": 6
    },
    "end": {
      "timeline": 0,
      "turn": 1,
      "player": "black",
      "coordinate": "f6",
      "rank": 6,
      "file": 6
    },
    "player": "black"
  });
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //User1 plays third action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"white","coordinate":"d1","rank":1,"file":4},"end":{"timeline":0,"turn":2,"player":"white","coordinate":"f3","rank":3,"file":6},"player":"white"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  await sleep(1000);
  
  //User2 plays four action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user2Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":2,"player":"black","coordinate":"d7","rank":7,"file":4},"end":{"timeline":0,"turn":2,"player":"black","coordinate":"d6","rank":6,"file":4},"player":"black"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user2Token).send({});
  await sleep(1000);
  
  //User1 plays fifth action
  res = await request.post(url + '/sessions/' + ses.id + '/move').set('Authorization', user1Token).send({"promotion":null,"enPassant":null,"castling":null,"start":{"timeline":0,"turn":3,"player":"white","coordinate":"f3","rank":3,"file":6},"end":{"timeline":0,"turn":3,"player":"white","coordinate":"h5","rank":5,"file":8},"player":"white"});
  await sleep(1000);
  res = await request.post(url + '/sessions/' + ses.id + '/submit').set('Authorization', user1Token).send({});
  ses = res.body;
  console.log('Session ended:');
  console.log(JSON.stringify(ses, null, 2));
  await sleep(1000);
};

main();
```

Live demo on JSFiddle available [here](https://jsfiddle.net/alexbay218/0hpo4wbL/)

## Installation

To run the server, clone this repo using `git clone https://gitlab.com/alexbay218/5d-chess-server`.

Run `cd 5d-chess-server` and then `npm install` to install dependencies.

Standard way of running the server would be to use `npm start`, however for some production environments, it may be beneficial to have the node process be restarted automatically on full crash. Running the command `npm run fstart` will use the [forever](https://www.npmjs.com/package/forever) npm package to keep the server running even on crashes.

## Config

In the default configuration, 5D Chess Server starts a local [NeDB](https://github.com/louischatriot/nedb) instance and runs on port 5000. To change this configuration, create a new file in the project root directory called `config.json` and copy and paste text inside `defaultConfig.json` into the `config.json` file.

The `config.json` file contains JSON data to be used as configuration settings. Here are the following fields available.

  - `db` - JSON Object or null, used to define what database to connect too. If null, it uses NeDB to create a local MongoDB compatible database instance.
    - `type` - String indicating what database driver to use (should be MongoDB compatible). Currently only 'mongodb' is implemented. Other fields with the sub-object may be different depending on this field.
    - If `type === 'mongodb'`, following fields exist:
      - `url` - MongoDB connection string (https://docs.mongodb.com/manual/reference/connection-string/).
      - `options` - JSON Object for MongoDB connection options (http://mongodb.github.io/node-mongodb-native/2.2/reference/connecting/connection-settings/).
  - `saltRounds` - Number indicating cost factor for bcrypt salting when hashing passwords. Higher is more secure, but will decrease performance. Default of 10 is used. More information available here (https://stackoverflow.com/questions/4443476/optimal-bcrypt-work-factor).
  - `heartbeat` - Number indicating the interval in milliseconds for the server heartbeat function to run. Default of 60000 (1 minute). This is used to run cleanup operations periodically.
  - `archive` - Number indicating the amount of time after a session has ended for the server to wait before removing it. Defaults of 300000 (5 minutes).
  - `deadline` - Number indicating the amount of time to wait for session to finish before terminating it (regardless if it has started or not). Default of 604800000 (1 week).
  - `startingElo` - Number indicating the starting ELO of new players
  - `enableBrute` - Boolean indicating whether to enable rate limiting functionality. Default `false` for local testing purposes, but it should be `true` on production environments.
  - `port` - Number indicating network port to run the server on. Default of 5000. If the `PORT` environment variable is set, it will use that value instead.

## API

For all routes unless otherwise specified, the data should be in the form of JSON data (`Content-Type` header should have the value of `application/json`).

For routes requiring user authorization, JWT is required for authentication. These tokens have a max lifetime of 12 hours. To authenticate, the `Authorization` header should have the value of the JWT string (Example: `{ "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c" }`). Authentication failures will return HTTP status code of `401 Unauthorized` (no other route will return this status code). Authorization failures will return HTTP status code of `403 Forbidden`.

On error, the server will return error data in the form of JSON data with the error field containing  error information.

Default user of 'admin' is created on startup, with full ability to do all actions. When displaying all users, it is advise to skip the admin user (will always have 'admin' username).

### Authentication

**POST /register**

Endpoint to register new users into the system. No JWT authentication needed.

By default, registering users via regular HTTP requests results in the user being labeled as a bot. This has no functional impact other than notifying other users that the user in question is most likely automated. To integrate with clients designed for end users, contact the server admin for more information regarding registering non-bot users. For others running new instances of this server, contact the author for more information regarding registering non-bot users. This system is very weak and is only meant to make bot/engine developers aware that they should register as a bot user.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Data** - JSON data containing the information for registering a user.

    - `username` - **[Required]** Username string containing only a-z, 0-9, _ and - characters. This must be unique (no two users can have the same username). Truncated to first 100 characters.
    - `password` - **[Required]** Password string used for login. Truncated to first 100 characters.
    - `bio` - String for user description. Truncated to first 500 characters.
    - `fullname` - String for full name of user. Truncated to first 100 characters.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).

  - **Response** - JWT for authentication in the form of a string.

**POST /login**

Endpoint to login from. No JWT authentication needed. Returns JWT for authentication purposes.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Data** - JSON data containing the information for logging in a user.

    - `username` - **[Required]** Username string containing only a-z, 0-9, _ and - characters. This must match existing user.
    - `password` - **[Required]** Password string used for login.

   - **Response** - JWT for authentication in the form of a string.

**GET /refreshToken**

Endpoint to obtain new token via old token (skip login process and get new token before old token expires). Requires JWT authentication. Returns new JWT for authentication purposes.

  - **Rate Limit** - Allows one request every 5 seconds.

  - **Response** - JWT for authentication in the form of a string.

### User

**GET /users/:username**

Endpoint to get a user's information. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing user information.

    - `username` - Unique string identifier.
    - `fullname` - String identifier meant for containing the full name of the user.
    - `bio` - String containing description of the user.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).
    - `bot` - Boolean indicating if user is registered as a bot or not.
    - `joinDate` - UNIX epoch time in milliseconds indicating when the user was registered.

**GET | POST /users**

Endpoint to get multiple users. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Data** - For use with **POST /users** request. Should be a MongoDB style query object as JSON.

  - **Response** - JSON data containing array of user information. For each user, the user object contains the same fields as **GET /users/:username**.

**POST /users/:username/update**

Endpoint to update user information. Requires JWT authentication. Only original user can update.

  - **Rate Limit** - Allows one request every 5 seconds

  - **Data** - JSON data containing new user information to update.

    - `fullname` - String identifier meant for containing the full name of the user.
    - `bio` - String containing description of the user.
    - `country` - ISO 3166-1 Alpha-3 compliant string to indicate user's country of origin (List here: https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3#Officially_assigned_code_elements).

  - **Response** - JSON data containing updated user information (same as **GET /users/:username**).

### Session

Sessions are active games in play.

**GET /sessions/:id**

Endpoint to get session data via session id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Response** - JSON data containing session data.

    - `id` - Unique string identifier.
    - `host` - Username of the user that created the session.
    - `white` - Username of the user/player playing white.
    - `black` - Username of the user/player playing black.
    - `variant` - String indicating which 5D Chess variant in play. Valid values are 'standard', 'defended_pawn', or 'half_reflected'.
    - `format` - String indicating timing format in use. Can be 'untimed', 'bullet', 'blitz', 'rapid', 'standard', 'tournament', or `<total minutes>:<total seconds>;incf<flat increment>;inct<timeline increment>` (see Timing section below).
    - `ranked` - Boolean indicating if session is ranked.
    - `ready` - Boolean indicating if non-host user/player is ready to play.
    - `offerDraw` - Boolean indicating if current player is offering draw. Resets on submit.
    - `started` - Boolean indicating if session has started.
    - `startDate` - UNIX epoch time in milliseconds indicating when the session was started.
    - `ended` - Boolean indicating if session has ended.
    - `endDate` - UNIX epoch time in milliseconds indicating when the session was ended.
    - `archiveDate` - UNIX epoch time in milliseconds indicating when the session will be archived.
    - `timed` - Object containing timing information (`null` if game is not timed).
      - `whiteDurationLeft` - Amount of time left for white to play.
      - `blackDurationLeft` - Amount of time left for black to play.
      - `startingDuration` - Amount of time to give to both players at the start of the game.
      - `perActionFlatIncrement` - Amount of time to give to player when their turn starts.
      - `perActionTimelineIncrement` - Amount of time to give to player when their turn starts (this scales per present timeline in play).
    - `board` - `Board` object representing current session (https://gitlab.com/alexbay218/5d-chess-js#schemas).
    - `actionHistory` - Array of `Action` objects representing the history of actions in current session (https://gitlab.com/alexbay218/5d-chess-js#schemas).
    - `moveBuffer` - Array of `Move` objects representing the moves played by the current player in the current session (https://gitlab.com/alexbay218/5d-chess-js#schemas).
    - `player` - String indicating the current player ('white' or 'black').
    - `winner` - String indicating the winning player ('white', 'black', or 'draw'). Will be `null` if current session has not ended yet.
    - `winCause` - String indicating the reason for winning ('timed_out', 'forfeit', or 'regular'). Stalemates results in 'regular', whereas draws results in 'forfeit'.

**GET | POST /sessions**

Endpoint to get multiple sessions. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Data** - For use with **POST /sessions** request. Should be a MongoDB style query object as JSON.

  - **Response** - JSON data containing array session object data. For each session, the session object contains the same fields as **GET /sessions/:id**.

**POST /sessions/new**

Endpoint to create new session data. Requires JWT authentication.

  - **Rate Limit** - Allows one request every second.

  - **Data** - JSON data containing session information to create a new session.

    - `player` - **[Required]** String indicating the side the current user is going to play ('white' or 'black').
    - `variant` - String indicating which 5D Chess variant to play (defaults to 'standard'). Valid values are 'standard', 'defended_pawn', or 'half_reflected'.
    - `format` - String indicating timing format to use. Can be 'bullet', 'blitz', 'rapid', 'standard', or 'tournament' (see Timing section below).
    - `timed` - Object containing timing information. Required if timed play is desired and `format` field is not provided.
      - `whiteDurationLeft` - Amount of time left for white to play.
      - `blackDurationLeft` - Amount of time left for black to play.
      - `startingDuration` - Amount of time to give to both players at the start of the game.
      - `perActionFlatIncrement` - Amount of time to give to player when their turn starts.
      - `perActionTimelineIncrement` - Amount of time to give to player when their turn starts (this scales per present timeline in play).
    - `ranked` - Boolean indicating if session is ranked. If ranked play is desired, `format` should be used instead of `timed`.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/remove**

Endpoint to remove a session via session id. Requires JWT authentication. Must be host.

  - **Rate Limit** - Allows one request every second.

  - **Data** - No data required.

  - **Response** - No data returned.

**POST /sessions/:id/addUser**

Endpoint to add a user as a player to a session data via session id. Requires JWT authentication. Must be host.

  - **Rate Limit** - Allows one request every second.

  - **Data** - JSON data containing user to add as second player

    - `username` - **[Required]** Username string of the user to add as second player.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/ready**

Endpoint to signal that the non-host player is ready to play in a session a via session id. Requires JWT authentication. Must be non-host player.

  - **Rate Limit** - Allows one request every second.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/unready**

Endpoint to signal that the non-host player is not ready to play in a session a via session id. Requires JWT authentication. Must be non-host player.

  - **Rate Limit** - Allows one request every second.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/start**

Endpoint to signal that the session has been started via session id. Requires JWT authentication. Must be host player.

  - **Rate Limit** - Allows one request every second.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/move**

Endpoint to make a move in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Data** - **[Required]** JSON of a valid `Move` object (https://gitlab.com/alexbay218/5d-chess-js#schemas).

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/undo**

Endpoint to undo a move in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/submit**

Endpoint to submit all moves played in moveBuffer in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/forfeit**

Endpoint to forfeit in an active session. Requires JWT authentication. User must be current player.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

**POST /sessions/:id/draw**

Endpoint to either offer or accept a draw in an active session. Requires JWT authentication. If user is current player, they offer a draw. If user is not current player, they accept draw request.

  - **Rate Limit** - Allows one request every 0.25 seconds.

  - **Data** - No data required.

  - **Response** - JSON data containing session data (same as **GET /sessions/:id**).

### Game

Games are previous sessions that have been played.

**GET /games/:id**

Endpoint to get game data via game id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing game data.

    - `id` - Unique string identifier.
    - `white` - Username of the user/player playing white.
    - `black` - Username of the user/player playing black.
    - `variant` - String indicating which 5D Chess variant was played. Valid values are 'standard', 'defended_pawn', or 'half_reflected'.
    - `format` - String indicating timing format used. Can be 'untimed', 'bullet', 'blitz', 'rapid', 'standard', 'tournament', or `<total minutes>:<total seconds>;incf<flat increment>;inct<timeline increment>` (see Timing section below).
    - `ranked` - Boolean indicating if game is ranked.
    - `startDate` - UNIX epoch time in milliseconds indicating when the game was started.
    - `endDate` - UNIX epoch time in milliseconds indicating when the game was ended.
    - `actionHistory` - Array of `Action` objects representing the history of actions in the game (https://gitlab.com/alexbay218/5d-chess-js#schemas).
    - `winner` - String indicating the winning player ('white', 'black', or 'draw').
    - `winCause` - String indicating the reason for winning ('timed_out', 'forfeit', or 'regular'). Stalemates results in 'regular', whereas draws results in 'forfeit'.

**GET | POST /games**

Endpoint to get multiple games. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Data** - For use with **POST /games** request. Should be a MongoDB style query object as JSON.

  - **Response** - JSON data containing array game object data. For each game, the game object contains the same fields as **GET /games/:id**.

### Ranking

Rankings are the calculated ELO ranking of a user from a ranked game. Multiple ranking objects of a user makes a history of how the user did in ranked play.

Note, not all users have rankings. Rankings only get created when user plays first ranked game. When users play their first ranked game, a 'starting' ranking with the starting ELO rating (default 1200) is created. This 'starting' ranking uses user join date as the date.

**GET /rankings/:id**

Endpoint to get ranking data via ranking id. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Response** - JSON data containing ranking data.

    - `id` - Unique string identifier.
    - `username` - Username of the user.
    - `variant` - String indicating which 5D Chess variant was played. Valid values are 'standard', 'defended_pawn', or 'half_reflected'.
    - `rating` - ELO rating of player calculated from starting game. Default starting ELO is 1200.
    - `format` - String indicating timing format used. Can be 'bullet', 'blitz', 'rapid', 'standard', or 'tournament'.
    - `date` - UNIX epoch time in milliseconds indicating when the ranking was calculated was calculated.
    - `gameId` - Corresponding id of the game that created this calculation. Empty string if ranking is the starting ELO.

**GET | POST /rankings**

Endpoint to get multiple rankings. No JWT authentication needed.

  - **Rate Limit** - Allows one request every second.

  - **Data** - For use with **POST /rankings** request. Should be a MongoDB style query object as JSON.

  - **Response** - JSON data containing array ranking object data. For each ranking, the ranking object contains the same fields as **GET /rankings/:id**.

## Timing

For the format string to indicating standard time controls, these are the following values:

  - 'bullet' - Starting duration of 5 minutes.
  - 'blitz' - Starting duration of 10 minutes with 5 second present timeline increments.
  - 'rapid' - Starting duration of 20 minutes with 10 second present timeline increments.
  - 'standard' - Starting duration of 40 minutes with 20 second present timeline increments.
  - 'tournament' - Starting duration of 80 minutes with 40 second present timeline increments.

## FAQ

### Is it any good?

Yes (maybe).

### You incorrectly evaluated this full board as a checkmate (or not a checkmate)!

If you can provide me an action list (object, json, or notation) or the board state, and submit it as an issue to this repo (https://gitlab.com/alexbay218/5d-chess-js), I can get right on it. This goes for any other bugs. A good way to verify if it is correct or not is to repeat the same moves in the same order in '5D Chess With Multiverse Time Travel' and see if it matches.

### Why is this on GitLab instead of GitHub?

I made the switch from GitHub to GitLab mid 2019 when I was starting a new long term project called KSS. Back then, GitHub did not have many of the features it does now, such as integrated CI/CD and more. GitLab was the superior product in almost every way. Furthermore, as a believer in the open source, it seem ironic that open source software would be hosted on closed source platforms. With GitLab being open source, I can be sure that if GitLab.org crumbles, I can still maintain the overall project structure via GitLab instances. This allows me to preserve the Git repo itself, but also the issues, labels, rules, pipelines, etc. that are fundamental to a project. With GitHub, developers do not have this guarantee and they also do not have full control over their project structure.

For a (biased, but not untrue) comparison, visit this link [here](https://about.gitlab.com/devops-tools/github/decision-kit.html)

### Isn't the game copyrighted?

Yes, the game '5D Chess With Multiverse Time Travel' is under copyright by Thunkspace, LLC and any source code, written works, and other copyrightable materials are the property of Thunkspace, LLC. However, copyright does not extend to an idea, which include game rules. So as long as the new work does not contain a direct copy of the rules or other material within the original game. Well known precedent for this is Hasbro's lawsuit against Scrabulous in which they dropped it after Scrabulous removed material that could possible be considered violating copyright (https://www.cnet.com/news/hasbro-drops-scrabulous-lawsuit/).

Also of note is this article from the American Bar Association (https://www.americanbar.org/groups/intellectual_property_law/publications/landslide/2014-15/march-april/its_how_you_play_game_why_videogame_rules_are_not_expression_protected_copyright_law/).

5D Chess Server in no way aims to violate any copyright laws, but instead aims to be an open source implementation of the original ideas as presented by '5D Chess With Multiverse Time Travel'.

## Copyright

All source code is released under AGPL v3.0 (license can be found under the `LICENSE` file).

Any addition copyrightable material not covered under AGPL v3.0 is released under CC BY-SA.
